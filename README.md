# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a Dockerfile of a base image for day-to-day bioinformatics research activities.

The image is based on miniconda3:4.8.2, which is the primary package management tool for the image, and includes the following tools and software packages:

* R
* R essential packages for computational biology, such as Rsamtools, Repitools, rtracklayer, etc.
* GEM tools
* samtools
* bedtools
* Snakemake
* And many more

### Usage

You can either build the Docker image from scratch using this Dockerfile, or pull the already-built image from Docker Hub:

```
docker pull zephyre/comp-bio:v0.3.1
```

For more details, please check out the [Docker Hub page](https://hub.docker.com/repository/docker/zephyre/comp-bio).
